package com.planari.koba.weatherwall;

import android.graphics.Bitmap;
import android.media.Image;

/**
 * Created by koba on 2015/03/26.
 */
public class WeekObject {
    public String date;
    public int tempMax;
    public int tempMin;
    public int temp;
    public double wind;
    public double rain;
    public String weather;
    public int id;
    public String icon;

    public WeekObject(int id, String date, String weather, int temp, int max, int min, double rain, double wind,String icon){
        this.id = id;
        this.date = date;
        this.weather = weather;
        this.temp = temp;
        this.tempMax = max;
        this.tempMin = min;
        this.wind = wind;
        this.rain = rain;
        this.icon = icon;
    }
}
