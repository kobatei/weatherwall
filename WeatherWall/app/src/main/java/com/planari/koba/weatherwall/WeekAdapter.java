package com.planari.koba.weatherwall;

import android.content.Context;
import android.provider.Contacts;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by koba on 2015/03/26.
 */
public class WeekAdapter extends ArrayAdapter<WeekObject> {
    private LayoutInflater layoutInflater;

    public WeekAdapter(Context context, int textViewResourceId,
                         List<WeekObject> objects) {
        super(context, textViewResourceId, objects);
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        WeekObject item = (WeekObject) getItem(position);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.week_grid_item,
                    null);
        }

        TextView date = (TextView) convertView.findViewById(R.id.grid_date_text);
        TextView rain = (TextView) convertView.findViewById(R.id.grid_rain_text);
        TextView max = (TextView) convertView.findViewById(R.id.grid_temp_max);
        TextView min = (TextView) convertView.findViewById(R.id.grid_temp_min);
        ImageView icon = (ImageView) convertView.findViewById(R.id.grid_weather_icon);
        RelativeLayout back = (RelativeLayout) convertView.findViewById(R.id.grid_back);


        date.setText(item.date);
        rain.setText(String.format("%.1f", item.rain) + " %");
        max.setText(String.format("%d°",item.tempMax));
        min.setText(String.format("%d°",item.tempMin));
        icon.setImageResource(ImageUtil.getImageResouce(item.icon));
        back.setBackgroundResource(CardUtil.cardSet(item.id));
//        icon.setImageBitmap(item.weatherIcon);

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
