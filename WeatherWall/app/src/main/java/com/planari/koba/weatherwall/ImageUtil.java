package com.planari.koba.weatherwall;

import android.widget.ImageView;

/**
 * Created by koba on 2015/03/27.
 */
public class ImageUtil {
    public static int getImageResouce(String iconid){
        int r = 0;
        if(iconid.equals("01d"))
            r = R.drawable.sun_d;
        else if(iconid.equals("01n"))
            r = R.drawable.sun_d;
        else if(iconid.equals("02d"))
            r = R.drawable.cloud_a_d;
        else if(iconid.equals("02n"))
            r = R.drawable.cloud_a_d;
        else if(iconid.equals("03d"))
            r = R.drawable.cloud_b_d;
        else if(iconid.equals("03n"))
            r = R.drawable.cloud_b_d;
        else if(iconid.equals("04d"))
            r = R.drawable.cloud_c_d;
        else if(iconid.equals("04n"))
            r = R.drawable.cloud_c_d;
        else if(iconid.equals("09d"))
            r = R.drawable.shower_d;
        else if(iconid.equals("09n"))
            r = R.drawable.shower_d;
        else if(iconid.equals("10d"))
            r = R.drawable.rain_d;
        else if(iconid.equals("10n"))
            r = R.drawable.rain_d;
        else if(iconid.equals("11d"))
            r = R.drawable.storm_d;
        else if(iconid.equals("11n"))
            r = R.drawable.storm_d;
        else if(iconid.equals("13d"))
            r = R.drawable.snow_d;
        else if(iconid.equals("13n"))
            r = R.drawable.snow_d;
        else if(iconid.equals("50d"))
            r = R.drawable.mist_d;
        else if(iconid.equals("50n"))
            r = R.drawable.mist_d;
        else
            r = R.drawable.mist_d;

        return r;
    }
}
