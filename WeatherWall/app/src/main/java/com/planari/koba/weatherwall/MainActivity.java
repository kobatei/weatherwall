package com.planari.koba.weatherwall;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */

    public static class PlaceholderFragment extends Fragment {
        private RequestQueue mRequestQueue;
        private static final Object TAG_REQUEST_QUEUE = new Object();
        private static final String REQUEST_URL_REQUEST_NOW_WEATHER_OBJECT
                = "http://api.openweathermap.org/data/2.5/forecast/city?units=metric&q=yokohama&APPID=e1d3e397cfee25bfb256c8e9a5a2365f";

        private TextView updateDate;
        private TextView nowTemp;
        public TextView nowWeather;
        private TextView nowRain;
        private TextView nowWind;
        private ImageView nowWeatherIcon;
        private GridView weekGrid;
        private RelativeLayout nowBack;
        private WeekAdapter adapter;
        private List<WeekObject> array;
        private Handler mHandler;
        private Timer mTimer;
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            updateDate = (TextView) rootView.findViewById(R.id.update_date);
            nowTemp = (TextView) rootView.findViewById(R.id.now_temp_text);
            nowWeather = (TextView) rootView.findViewById(R.id.now_weather_text);
            nowRain = (TextView) rootView.findViewById(R.id.now_rain_text);
            nowWind = (TextView) rootView.findViewById(R.id.now_wind_text);
            nowWeatherIcon = (ImageView) rootView.findViewById(R.id.now_weather_icon);
            weekGrid = (GridView) rootView.findViewById(R.id.week_grid);
            nowBack = (RelativeLayout) rootView.findViewById(R.id.now_back);
            array = new ArrayList<WeekObject>();
            adapter = new WeekAdapter(getActivity(), R.layout.week_grid_item, array);
            weekGrid.setAdapter(adapter);

            mTimer = new Timer();
            mHandler = new Handler();
            mRequestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());

            return rootView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstance) {
            super.onActivityCreated(savedInstance);
        }
        @Override
        public void onStart(){
            super.onStart();

            mTimer.schedule( new TimerTask(){
                @Override
                public void run() {
                    // mHandlerを通じてUI Threadへ処理をキューイング
                    mHandler.post( new Runnable() {
                        public void run() {
                            requestNowWeatherObject();
                        }
                    });
                }
            }, 0, 600000);
//            mHandler.post( new Runnable() {
//                public void run() {
//                    while(true){
//
//                        sleep();
//                    }
//
//
//                }
//            });

        }
        @Override
        public void onStop(){
            super.onStop();
            mTimer.cancel();
            mRequestQueue.cancelAll(TAG_REQUEST_QUEUE);
        }


        private void requestNowWeatherObject(){
            // リクエストを投げる
            JsonObjectRequest request = new JsonObjectRequest(
                    //GETのみ？
                    Request.Method.GET,
                    //リクエスト先
                    REQUEST_URL_REQUEST_NOW_WEATHER_OBJECT,
                    //リクエストパラメータ
                    null,
                    //レスポンス時のリスナー
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //レスポンス受け取り時の処理
                            if (response.toString() == ""){
                                //空の場合
                                return;
                            }

                            String date = null, weather  = null, icon = null;
                            int id = 0;
                            double rain = 0.0f, wind = 0.0f;
                            JSONObject obj = null, tmp = null;
                            List<WeekObject> list = new ArrayList<WeekObject>();

                            try {
                                JSONArray listArray = response.getJSONArray("list");
                                JSONArray mainArray, rainArray;
                                for (int i = 0; listArray.length() > i; i++){
                                    tmp = listArray.getJSONObject(i);
                                    date = tmp.getString("dt_txt");

                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Date formatDate = sdf.parse(date);
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(formatDate);
                                    cal.add(Calendar.HOUR, +9);
                                    date = sdf.format(cal.getTime());


                                    weather = tmp.getJSONArray("weather").getJSONObject(0).getString("description");
                                    id = tmp.getJSONArray("weather").getJSONObject(0).getInt("id");
                                    icon = tmp.getJSONArray("weather").getJSONObject(0).getString("icon");
                                    obj = tmp.getJSONObject("main");
                                    if(!tmp.isNull("rain") && !tmp.isNull("3h")){
                                        rain = tmp.getJSONObject("rain").getDouble("3h");
                                    }
                                    if(!tmp.isNull("wind")){
                                        wind = tmp.getJSONObject("wind").getDouble("speed");
                                    }

                                    list.add(new WeekObject(id,
                                            date,
                                            weather,
                                            (int)obj.getDouble("temp"),
                                            (int)obj.getDouble("temp_max"),
                                            (int)obj.getDouble("temp_min"),
                                            wind,
                                            rain,
                                            icon));
                                }

                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }


                            WeekObject weekObj = list.get(0);
                            nowWeather.setText(weekObj.weather);
                            nowTemp.setText(String.format("%d", (int)weekObj.temp));
                            nowWind.setText(String.format("%.1f", weekObj.wind)+" km/h");
                            nowRain.setText(String.format("%.1f", weekObj.rain) + " %");
                            nowBack.setBackgroundResource(CardUtil.cardSet(weekObj.id));
                            nowWeatherIcon.setImageResource(ImageUtil.getImageResouce(weekObj.icon));

                            SimpleDateFormat sdf2 = new SimpleDateFormat("HH時mm分");
                            updateDate.setText(sdf2.format(new Date()) + " 更新");

                            list.remove(0);
                            adapter.clear();
                            adapter.addAll(list);
                            adapter.notifyDataSetChanged();
                            //
                            Toast.makeText(getActivity().getApplicationContext(), weekObj.date, Toast.LENGTH_LONG).show();

                        }
                    },
                    //エラー時のリスナー
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            //エラー時の処理
                            Toast.makeText(getActivity().getApplicationContext(), "onErrorResponse", Toast.LENGTH_LONG).show();

                        }
                    }
            );
            //タグを設定する
            request.setTag(TAG_REQUEST_QUEUE);

            //リクエスト＆レスポンス情報の設定を追加
            mRequestQueue.add(request);

            //リクエスト開始
            mRequestQueue.start();

        }
    }
}
