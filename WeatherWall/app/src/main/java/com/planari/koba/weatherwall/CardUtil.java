package com.planari.koba.weatherwall;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.widget.RelativeLayout;

/**
 * Created by koba on 2015/03/27.
 */
public class CardUtil {
    public static int cardSet(int id){
        int d = 0;
        if(id == 800){
            //sunny
            d = R.drawable.card_view_design_sunney;
        }
        else if(id == 801){
            //uuuum
            d = R.drawable.card_view_design_uuuum;
        }
        else if(id == 802 || id == 803 || id == 804){
            //cloud
            d = R.drawable.card_view_design_cloudy;
        }
        else if(id / 100 == 5 || id / 100 == 3){
            //rain
            d = R.drawable.card_view_design_rainy;
        }
        else if(id / 100 == 6){
            //snow
            d = R.drawable.card_view_design_snow;
        }
        else if(id / 100 == 2){
            //storm
            d = R.drawable.card_view_design_storm;
        }
        else{
            //uuuuum
            d = R.drawable.card_view_design_uuuum;
        }

        return d;
    }
}
